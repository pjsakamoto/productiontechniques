﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwlAudio : MonoBehaviour
{
    [SerializeField] Animator anim;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip wingsSource;
    [SerializeField] AudioClip screechSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (anim.GetBool("isAttacking") == false)
        {
            owlWings();
        }
        if (anim.GetBool("isAttacking") == true)
        {
            owlScreeching();
        }
    }

    void owlWings()
    {
        audioSource.clip = wingsSource;
        if (!audioSource.isPlaying)
            audioSource.PlayOneShot(wingsSource);
    }

    void owlScreeching()
    {
        audioSource.clip = screechSource;
        if (!audioSource.isPlaying)
            audioSource.PlayOneShot(screechSource);
    }
}
