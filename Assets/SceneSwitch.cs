﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSwitch : MonoBehaviour
{
    public Image fadePanel;
    public float fadeTime;
    public TriggerZoneEventTest tze;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        PanelFadeIn();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            tze.UpdateZoneEvent();
            //Fade the panel to hide the scene change
            PanelFadeOut();
            //Change the scene after panel fade
            Invoke("ChangeScene", (fadeTime + 0.1f));

        }
    }

    private void ChangeScene() 
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void PanelFadeOut() 
    {
 
        // set the fade color
        Color fadeColor = Color.black;

        // set other fade vars - alpha refers to transparency
        float startAlpha = 0f;
        float endAlpha = 1f;
        StartCoroutine(FadeCoroutine(fadeColor, startAlpha, endAlpha, fadeTime));

    }

    private void PanelFadeIn() 
    {

        // set the fade color
        Color fadeColor = Color.black;

        // set other fade vars - alpha refers to transparency
        float startAlpha = 1f;
        float endAlpha = 0f;
        StartCoroutine(FadeCoroutine(fadeColor, startAlpha, endAlpha, 1f));


    }

    IEnumerator FadeCoroutine(Color fadeColor, float startAlpha, float endAlpha, float fadeTime)
    {
        Color startColor = fadeColor;
        if (startAlpha >= 0f)
        { // if startAlpha is -1f, that means just use whatever's there
            startColor.a = startAlpha;
        }
        else
        {
            startColor = fadePanel.color;
        }
        fadeColor.a = endAlpha;
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / Mathf.Max(0.001f, fadeTime); // Math.Max to prevent divide by zero error
            fadePanel.color = Color.Lerp(startColor, fadeColor, t);
            yield return 0;
        }
        Debug.Log("Fade Complete");
    }
}
