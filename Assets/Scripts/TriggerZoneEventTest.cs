﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;

public class TriggerZoneEventTest : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //Original Analyitics Code
        /*
        if (other.tag == "Player")
        {
            AnalyticsResult analyticsResult = Analytics.CustomEvent("LevelPassed");
            NoDestroyOnLoad.LevelsCompleted += 1;
            //Debug.Log("LevelMiddle Analytics Event: " + analyticsResult);

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        */
    }

    public void UpdateZoneEvent() 
    {
        AnalyticsResult analyticsResult = Analytics.CustomEvent("LevelPassed");
        NoDestroyOnLoad.LevelsCompleted += 1;
    }
}
