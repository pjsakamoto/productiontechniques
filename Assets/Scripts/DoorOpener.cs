﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class DoorOpener : MonoBehaviour
{
    // Start is called before the first frame update
    [Header("Door Movement")]

    //Angle for door to open
    public float openAngle;
    //Speed for Door Open
    public float openSpeed;

    //Store the original orienation of the door
    float defaultAngle;
    float endAngle;

    [Header("Player Input")]
    //Passphrase for door
    public string passphrase;

    public bool passphraseEntered;

    [Header("Misc")]
    //Trigger Area for Door
    public Collider triggerZone;

    //Display Prompt
    [SerializeField]
    bool inTriggerZone;

    bool doorOpened = false;

    bool opening = false;

    [Header("Russian Input")]
    public GameObject virtualKeyboard;
    public Text textInput;
    [Header("English Input")]
    public GameObject inputArea;
    public InputField answerField;
    void Start()
    {
        defaultAngle = transform.eulerAngles.y;
        endAngle = transform.eulerAngles.y + openAngle;

        //defaultAngle = transform.eulerAngles.y;
        //endAngle = transform.eulerAngles.y + openAngle;

    }

    // Update is called once per frame
    void Update()
    {
        if (inTriggerZone)
        {

          
            if (passphraseEntered)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                if (Input.GetKeyDown(KeyCode.I) && doorOpened == false)
                {
                    opening = true;

                }
            }
            else
            {
                //Commented Out Section is For Original English Input
                //inputArea.SetActive(true);
                //answerField.Select();

                //Russian Virtual Keyboard Input
                virtualKeyboard.SetActive(true);

                //Activate Keyboard Controls
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }

        }

        MoveDoor();

    }

  

    private void MoveDoor() 
    {
        Quaternion endPos = Quaternion.Euler(-90, endAngle, 0);
        if (opening)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, endPos, Time.deltaTime * openSpeed);
        }
       
        if (transform.rotation == endPos)
        {
            doorOpened = true;
        }
        
    
    }

    public void CheckAnswer() 
    {
        //Original English Input
        /*if (answerField.text.ToLower() == passphrase.ToLower())
        {
            passphraseEntered = true;
            inputArea.SetActive(false);
        }*/

        //Virtual Keyboard Input Russian
        if (textInput.text.Trim() == passphrase.Trim())
        {
            passphraseEntered = true;
            virtualKeyboard.SetActive(false);
        }
        else
        {
            virtualKeyboard.GetComponent<VirtualKeyboard>().ResetKeyboard();
        }


    }

    void OnGUI()
    {
        if (inTriggerZone && doorOpened == false && passphraseEntered)
        {
            GUI.Label(new Rect(Screen.width / 2 - 75, Screen.height - 100, 150, 30), "Press 'I' to open the door");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            inTriggerZone = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "Player")
        {
            inTriggerZone = false;
            //inputArea.SetActive(false);
            virtualKeyboard.SetActive(false);

            //Deactivate Keyboard Controls
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

        }

    }

}
