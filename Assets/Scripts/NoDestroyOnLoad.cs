﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoDestroyOnLoad : MonoBehaviour
{
    public static int deathCount = 0;
    [SerializeField] int deathCountVar;

    public static int LevelsCompleted = 0;
    [SerializeField] int levelsCompletedVar;
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void Update()
    {
        deathCountVar = deathCount;
        levelsCompletedVar = LevelsCompleted;
    }
}
