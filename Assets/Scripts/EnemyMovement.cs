﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private float speed;
    [SerializeField]
    private GameObject player;
    private Player playerScript;

    void Start()
    {
        //initialize variables
        speed = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        //step of the MoveTowards is the speed times the game time
        float step = speed * Time.deltaTime;

        //have the enemy sprite move towards the player object
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, step);
    }

}
