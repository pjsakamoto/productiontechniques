﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class VirtualKeyboard : MonoBehaviour
{
    public string typeText;

    public Text textBox;

    public GameObject upperCaseKeyboard;

    public GameObject lowerCaseKeyboard;

    public bool uppercase = true;
    int charCount = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (charCount > 0 )
        {
            textBox.text = typeText;
        }
        else
        {
            textBox.text = "Type Answer";
        }

        UpdateCase();
    }

    public void AddLetter(string c) 
    {
        if (charCount == 0)
        {
            typeText = "";
        }
        typeText += c;
        charCount++;
        AutomaticCase();
    }

    public void Backspace() 
    {
        if (charCount > 0)
        {
            string removeLast = typeText.Substring(0, typeText.Length - 1);
            typeText = removeLast;
            charCount--;
        }
        textBox.text = typeText;
        AutomaticCase();
    }

    public void ChangeCase() 
    {
        uppercase = !uppercase;
    }

    private void AutomaticCase() 
    {
        if (charCount >= 1)
        {
            uppercase = false;
        }
        if (charCount == 0)
        {
            uppercase = true;
        }
    }

    private void UpdateCase() 
    {
        if (uppercase)
        {
            upperCaseKeyboard.SetActive(true);
            lowerCaseKeyboard.SetActive(false);
        }

        if (uppercase == false)
        {
            lowerCaseKeyboard.SetActive(true);
            upperCaseKeyboard.SetActive(false);
        }
    
    }

    public void ResetKeyboard() 
    {

        StopCoroutine("ColourFlash");
        StartCoroutine("ColourFlash");
    }

    IEnumerator ColourFlash() 
    {
        Color orig = textBox.color;
        textBox.color = Color.red;
        yield return new WaitForSeconds(.5f);
        typeText = "";
        charCount = 0;
        AutomaticCase();
        textBox.color = orig;
        yield return null;


    }

}
