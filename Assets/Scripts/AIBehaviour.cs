﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIBehaviour : MonoBehaviour
{
    // References
    NavMeshAgent agent;
    Transform player;
    GameObject playerObj;
    Transform body;
    public LayerMask whatIsGround, whatIsPlayer;
    Animator anim;

    // Pathfinding
    [Header("Pathfinding")]
    public float waypointRange = 15f;
    Vector3 waypoint;
    float distanceToWaypoint;
    bool waypointSet;

    // Detection
    [Header("Detection")]
    public float sightRange, attackRange;
    bool playerInSight, playerInAttackRange;

    // Attack
    [Header("Attack")]
    [SerializeField] float attackRate;
    bool alreadyAttacked;
    [SerializeField] int attackDmg;

    void Awake()
    {
        // References
        player = GameObject.Find("Player").transform;
        playerObj = GameObject.Find("Player");
        body = transform.Find("Owl Body");
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
    }
    void Start()
    {
        alreadyAttacked = false;
    }

    void Update()
    {
        // Check for sight and attack range
        playerInSight = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        // Change later to body.transform, make script that moves body in y-axis when chasing
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        if (!playerInSight && !playerInAttackRange) Patrolling();
        if (playerInSight && !playerInAttackRange) Chase();
        if (playerInSight && playerInAttackRange) Attack();

    }

    void Patrolling()
    {
        anim.SetBool("isAttacking", false);

        if (!waypointSet) SetNewWaypoint();
        if (waypointSet)
        {
            agent.SetDestination(waypoint);

            // Detects if owl is stuck, and sets a new waypoint
            if (agent.speed == 0f)
            {
                float timePatrolling = 0;
                timePatrolling += Time.deltaTime;
                if (timePatrolling <= 7f) SetNewWaypoint();
            }
        }

        if (Vector3.Distance(transform.position, waypoint) < 2f) 
            waypointSet = false;
    }
    void SetNewWaypoint()
    {
        // Calculate random point in range
        float randomZ = Random.Range(-waypointRange, waypointRange);
        float randomX = Random.Range(-waypointRange, waypointRange);
        float randomY = Random.Range(-waypointRange, waypointRange);

        waypoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        // Waypoint reached
        if (Physics.Raycast(waypoint, -transform.up, 1f, whatIsGround))
            waypointSet = true;
    }

    void Chase()
    {
        agent.SetDestination(player.position);
    }

    void Attack()
    {
        agent.SetDestination(transform.position);
        transform.LookAt(player);
        anim.SetBool("isAttacking", true);
        //GetComponent<AudioSource>().clip = screech;

        if (!alreadyAttacked)
        {
            // Attack code here
            playerObj.GetComponent<Player>().TakeDamage(attackDmg);

            alreadyAttacked = true;
            Invoke( nameof(ResetAttack), attackRate);
        }
    }
    void ResetAttack()
    {
        alreadyAttacked = false;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, waypoint);
    }
}
