﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InspectBook : MonoBehaviour
{
    [SerializeField]
    //Is the player near the book
    private bool bookHover;

    //The content of this book
    public TextAsset wordPuzzle;

    //The "interpreter" that processes the test on the UI
    private OpenBook bookReader;

    //Is the book open or closed
    private bool bookOpen;

    //Material to show highlight
    public Material glowMaterial;

    //How far the raycast is
    public float rayDist;

    //Book Name
    public string bookName;

      
    // Start is called before the first frame update
    void Start()
    {
        bookOpen = false;
        //Find the "interpreter"
        bookReader = GameObject.FindObjectOfType<OpenBook>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I) && bookHover == true)
        {
            if (!bookOpen)
            {
                bookOpen = true;
                bookReader.ReadBook(wordPuzzle);
            } 
            else 
            {
                bookOpen = false;
                bookReader.CloseBook();
            }
            
        }
    }

    private void FixedUpdate()
    {
        //Changed detection to trigger
        //SphereHoverCheck();
    }

    void OnGUI()
    {
        if (bookHover == true)
        {
            if (!bookOpen)
            {
                //Display the name of the book and instructions to open
                GUI.Label(new Rect(Screen.width / 2 - 75, Screen.height - 100, 150, 50), "Press I to read " + bookName);
            }
            else
            {
                //Display the name of the book and instructions to close
                GUI.Label(new Rect(Screen.width / 2 - 75, Screen.height - 100, 150, 50), "Press I to close " + bookName);
            }
            
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Near Book");

            //Sets the variable that the player is near the book
            bookHover = true;

            //Highlight book so player knows its selected
            ApplyHighlightEffect();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
           
            //The player is no longer near the book
            bookHover = false;

            //Remove Highlight from Book
            RemoveHighlightEffect();
        }
    }
    private void ApplyHighlightEffect() 
    {
        //Get the mesh renderer
        MeshRenderer meshRend = this.gameObject.GetComponent<MeshRenderer>();

        //Set a new set of materials with the highlight material
        Material[] matArr = { meshRend.materials[0], glowMaterial, };

        //Apply the new material array to the mesh, overlays the meshes to produces "glow" effect
        meshRend.materials = matArr;
        
    }

    private void RemoveHighlightEffect()
    {
        //Get the mesh renderer
        MeshRenderer meshRend = this.gameObject.GetComponent<MeshRenderer>();

        //Set a new set of materials without the highlight material
        Material[] matArr = {meshRend.materials[0]};

        //Apply the new material array to the mesh
        meshRend.materials = matArr;

    }

    #region Physics Checks
    private void SphereHoverCheck() 
    {
        //New code for books on tables


        RaycastHit hit;

        if (Physics.SphereCast(transform.parent.position,rayDist,transform.parent.TransformDirection(Vector3.zero),out hit,1f))
        {
            //For checking if the script is working visually
           
            Debug.Log("Near Book");

            //Sets the variable that the player is near the book
            bookHover = true;

            //Highlight book so player knows its selected
            ApplyHighlightEffect();
        }
        else
        {
            Debug.Log("Away from book");
            //The player is no longer near the book
            bookHover = false;

            //Remove Highlight from Book
            RemoveHighlightEffect();
        }
    }

    private void LineHoverCheck() 
    {
        //This code is from when the interactable books were on the shelves
        RaycastHit hit;

        // Checks if the player is next to the book
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, rayDist))
        {

            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.right) * hit.distance, Color.yellow);
            Debug.Log("Hit");

            //Sets the variable that the player is near the book
            bookHover = true;

            //Highlight book so player knows its selected
            ApplyHighlightEffect();

        }

        //For Debugging during development
        else
        {
            //Debug Code
            /*Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.right) * 2f, Color.white);
            Debug.Log("Did not Hit");*/

            //The player is no longer near the book
            bookHover = false;

            //Remove Highlight from Book
            RemoveHighlightEffect();
        }


    }
    #endregion




}
