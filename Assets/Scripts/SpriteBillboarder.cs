﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteBillboarder : MonoBehaviour
{
    [SerializeField]
    private Camera gameCam;
    [SerializeField]
    private bool useStaticBillboard;

    // Start is called before the first frame update
    void Start()
    {
        //initialize variables
        //get the main camera for the billboarding view
        gameCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        //if useStaticBillboard is made true, then the sprites don't bend away when the camera gets close
        //if it's kept false then the sprites will rotate according to how close the camera gets
        if (!useStaticBillboard)
        {
            transform.LookAt(gameCam.transform);
        }
        else
        {
            transform.rotation = gameCam.transform.rotation;
        }

        //apply the rotation to the sprites rotation
        transform.rotation = Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0);
    }
}
