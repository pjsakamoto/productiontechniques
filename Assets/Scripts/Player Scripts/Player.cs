﻿using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] private HPMeter hPBar;
    [SerializeField] private int maxHealth;
    public int currentHealth;
    public static int deathCount = 0;
    
    //[SerializeField]
    //private StaminaMeter staminaBar;
    //private int maxStamina;
    //private int currentStamina;
    
    //private bool isUpArrowUp;

    // Start is called before the first frame update
    void Start()
    {
        //initialize variables
        //maxStamina = 100;
        currentHealth = maxHealth;
        if (hPBar != null)
            hPBar.SetMaxHealth(maxHealth);

        //currentStamina = maxStamina;
        //staminaBar.SetMaxStamina(maxStamina);
        //isUpArrowUp = true;
    }

    void Update()
    {
        //for testing the health bar
        if (Input.GetKey(KeyCode.F))
        {
            TakeDamage(1);
        }
        if (Input.GetKey(KeyCode.Return))
        {
            ReviveHealth(1);
        }

        /*for testing the stamina bar
        if (Input.GetKey(KeyCode.UpArrow))
        {
            isUpArrowUp = false;
            UseStamina(1);
        }
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            isUpArrowUp = true;
        }
        if (isUpArrowUp == true && currentStamina < maxStamina)
        {
            //if the player lets go of the up arrow, their stamina will reload
            RegainStamina(1);
        }
        */

    }

    public void ReviveHealth(int health)
    {
        currentHealth += health;
        if (hPBar != null)
            hPBar.SetHealth(currentHealth);

        //keeps the max health at 100
        if (currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
        }
    }
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        if (hPBar != null)
            hPBar.SetHealth(currentHealth);
        
        //keeps the current health at 0
        if(currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }
    }

    public void Die()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        // Send Analytics Event
        AnalyticsResult analRes = Analytics.CustomEvent("PlayerDeath");
        if (analRes == AnalyticsResult.Ok)
        {
            NoDestroyOnLoad.deathCount += 1;
            Debug.Log("PlayerDeath Analytics Event: " + analRes);
        } 
        else
        {
            Debug.Log("FUCKKKKKKK");
        }
    }

    //public void RegainStamina(int energy)
    //{
    //    currentStamina += energy;
    //    staminaBar.SetStamina(currentStamina);

    //    //keeps the max stamina at 100
    //    if (currentStamina >= maxStamina)
    //    {
    //        currentStamina = maxStamina;
    //    }
    //}


    //public void UseStamina(int energy)
    //{
    //    currentStamina -= energy;
    //    staminaBar.SetStamina(currentStamina);

    //    //keeps the current stamina at 0
    //    if (currentStamina <= 0)
    //    {
    //        currentStamina = 0;
    //    }
    //}

    //void OnCollisionStay(Collision collision)
    //{
    //    //if the enemy object collides then the player takes damage
    //    if (collision.gameObject.tag == "Enemy")
    //    {
    //        Debug.Log("collided");
    //        TakeDamage(1);
    //    }
    //}
}
