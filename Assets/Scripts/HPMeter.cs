﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPMeter : MonoBehaviour
{
    [SerializeField]
    private Slider slider;

    public void SetMaxHealth(int health)
    {
        if (slider != null)
        {
            //variables are set in the player script
            slider.maxValue = health;
            slider.value = health;
        }
        else if (slider == null)
        {
            Debug.Log("Health Slider not found!");
        }
    }

    public void SetHealth(int health)
    {
        if (slider != null)
        {
            slider.value = health;
        }
        else if (slider == null)
        {
            Debug.Log("Health Slider not found!");
        }
    }
}
