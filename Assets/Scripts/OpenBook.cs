﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenBook : MonoBehaviour
{
    [SerializeField]
    private Text bookText;

    [SerializeField]
    private GameObject bookPages;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReadBook(TextAsset puzzle) 
    {
        bookText.text = puzzle.text;
        bookPages.SetActive(true);
    }

    public void CloseBook()
    {
        bookText.text = null;
        bookPages.SetActive(false);
    }


}
