﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScreen : MonoBehaviour
{

    public void LoadGame()
    {
        Debug.Log("Game Loaded");
        //load my proto scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ExitGame()
    {
        Debug.Log("App Exited");
        //exit the game application
        Application.Quit();
    }
}
